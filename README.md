# Billator

A quick and dirty way to create bills in HTML/PDF format from a list of task with duration in days.

Written in PHP.

May be adapted in the futur to allow for more use cases.

Currently support only the french language but you can write your own template that support your language.

Long term goal is to include this code into a bigger project that manage all the life of a freelancer

## Requirements

- >= PHP 8.0 (not tested with older versions but may work)
- Wkhtmltopdf https://wkhtmltopdf.org/index.html

## Installation

- Run `composer install`

## Usage

- Copy `bill.example.yaml` and create your own Bill using the structure

- If you want you can put your taks in a text file with this format:

```
// file tasks.txt
Foo bar n°1 // task name
0.8 + 2 + 5  // task duration

Foo bar n° 2
3

==== // new category
Foo bar n° 3
4
```

Then generate the categories in a yaml format with the script `generate_categories.php`

- Generate the HTML using the script `generate_html.php`

- Then finally generate the PDF with this cmd :

`wkhtmltopdf bill.html bill.pdf`

