<?php

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Twig\Extra\Intl\IntlExtension;

require 'vendor/autoload.php';

$data = Yaml::parseFile('./bill.yaml');

$totalDays = 0;
foreach ($data['categories'] as $i => $category) {
  $sub = 0;
  foreach ($category['tasks'] as $task) {
    $sub += $task['days'];
  }
  $totalDays += $sub;
  $data['categories'][$i]['total'] = $sub;
}

$total = ceil($totalDays * $data['daily_rate']);
$alreadyPayed = false;
$data['summary'] = [
  'days_total' => $totalDays,
  'total' => $total,
  'already_payed' => $alreadyPayed ? ceil($alreadyPayed) : false,
  'remaining' => ceil($total - ($alreadyPayed ? ceil($alreadyPayed) : 0))
];

$loader = new \Twig\Loader\FilesystemLoader('./templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$twig->addExtension(new IntlExtension());

$template = $twig->load('bill.html.twig');
$html = $template->render($data);

echo $html;
