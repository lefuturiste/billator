<?php

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

require 'vendor/autoload.php';

$r = file_get_contents('./tasks.txt');
$lines = explode("\n", $r);

$expr = new ExpressionLanguage();

$days = [];
$categories = [];
$items = [];
$currentItem = [];
for ($i = 0; $i < count($lines); $i++) {
  $line = $lines[$i];
  if (
    empty($line) ||
    str_starts_with($line, '#') ||
    str_starts_with($line,'//')
  ) {
    continue;
  }
  if (str_starts_with($line, '==')) {
    $categories[] = ['name' => 'X', 'tasks' => $items];
    $items = [];
    $currentItem = [];
    continue;
  }
  $currentItem[] = $lines[$i];
  if (count($currentItem) == 2) {
    $days = $expr->evaluate($currentItem[1]);
    $total += $days;
    $items[] = [
      'name' => $currentItem[0],
      'days' => $days
    ];
    $currentItem = [];
  }
}
$categories[] = ['name' => 'X', 'tasks' => $items];

var_dump(count($categories));
echo $total . "\n";

$yaml = Yaml::dump(['categories' => $categories], 5);
file_put_contents('./categories.yaml', $yaml);

